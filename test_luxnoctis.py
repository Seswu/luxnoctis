"""
Test module.

All *test* functions are executed automatically by pytest.
Any @pytest.fixture functions, when named as parameter, brings in the result
of the fixture function as parameter for the given test function.
"""
# pylint: disable=redefined-outer-name, no-name-in-module
#         ref-out-name:
#         Pytest does some automatic decoration for ease of use;
#         presumably this is what pylint catches on to and complains
#         about.
#         no-name-in:
#         Pylint is unable to locate perfectly normal members of perfectly
#         normal modules. Explanations for this remain elusive.

# Standard libraries
import faulthandler
import logging
import pytest

# Internal code
import luxnoctis.layer_data as layer_data
import luxnoctis.layer_function as layer_function


@pytest.fixture(scope="function")
def setup():
    """
    Populates stack with initial data for later testing.
    """
    faulthandler.enable(open("core.dump", 'w'))
    observer = layer_function.Observer()
    stack = layer_data.Stack(observer=observer)
    stack.push("One Two Tree")
    stack.push("Caravan Mitigates Cardigan")
    stack.push("One up for the cohesion")
    stack.push("Random winds passes true byly")
    stack.push("More poets soar higher")
    stack.push("Stacks conveniently storage sticks")
    stack.push("Nighttime begets darkness")
    stack.push("Sunshine sears like spear 'cross feather")
    stack.push("Box flies like box")
    testdata = {'gaffel':
                {456:
                 {'megannon':
                  {'galymedies':
                   {5, 6},
                   'dinnerfork': [3, 4, 5, 6, 'e']}}},
                'plate': 'ozymandias',
                'carmine': 'belle du fleur'}
    stack.push(testdata)
    testdata = (
        "As we walk through the darkness towards the legends of old, "
        "soft misty vapors follow us through the fog so insipedly "
        "flowing around the ruins of the ancients..")
    stack.push(testdata)
    testdata = ['Meeple', 3, 3984569, [2, 3, 4, 56, 0], "Boia",
                ['Sintilascape', 5],
                {'nilia': 'keyscape', 5: 'almond'}]
    for data in testdata:
        stack.push(data)
    return stack


def test_pop_all_stack_stacklist(setup):
    """
    Test if the lists in the stack class are emptied out in parallel,
    and that they are initialized with the same length.
    """
    teststack = setup
    while teststack.stacklist:
        element = teststack.pop()
        logging.debug(element)
    assert teststack.stacklist == []
    assert teststack.selected == []


def test_pop_all_stack_selected(setup):
    """
    Test if the lists in the stack class are emptied out in parallel,
    and that they are initialized with the same length.
    """
    teststack = setup
    while teststack.selected:
        element = teststack.pop()
        logging.debug(element)
    assert teststack.stacklist == []
    assert teststack.selected == []


def test_stash(setup):
    """
    'Random' test: Check if an element is what it is supposed to be.
    """
    stack = setup
    logging.debug(stack)
    assert stack.stacklist[0] == "One Two Tree"
