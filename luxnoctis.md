Lux Noctis
===========

**The planning of the beast**

-----------------------------------------------------------------------------

Problem Tree
-------------

- "Lux Noctis"
    - Plugin types
        - Source
        - Filter
        - Classifier
        - Sorter
        - Conversion
        - Viewer
        - Editor
        - 'Working memory'?
    - Architecture
        - Plugin system
            - Variation of "Chain of Responsibility"
            - Each plugin is a function
            - Plugins are loaded to a list
            - GUI  
            List gets searched for fitting functions at menu-click
            - Command line  
            Plugin functions as well as system functions can be called by reverse polish notation
            from system commmand line also
    - MVP
        - Best to have minimum scope
        - Along the lines of what I have already
        - As little fancy as possible
        - Collaboration and groups only comes to their right when there is a community/group of invested users; established users that experiment or new group who wants to test a proven concept
        - So it needs to be useful for the individual
        - Brainstorm editor was an idea; use editor for notes and use stack(s) for sorting in them manually, into themes/projects/tasks etc
          - Too graphical and ui-heavy though; I don't know the innards of Qt well enough
        - Note Organizer
          - Use editor to make notes; categorize with hierarchical tags
          - Use stack to sort and order notes
          - Can be used as a prioritized task list
          - Can be used to sort knowledge and instructions arriving at random
          - Could easily be extended so that new things arrived from ie discord as source, in order to be sorted
          - Could be extended with formatter to produce document or static webpage
          - Design
            - Stack of notes to the left
            - Note editor to the right
            - Top of stack has buttons so as to sort according to <priorities and eg fifo-capability> and filter according to search criteria <'can be done at home', 'involves kittens', etc>
            - Treeview better for structuring notes? Yes, if hierarchy.
            - Stack better for sorting notes when.. prioritization. And when it's a todo list.
            - So what I'd really like is to seamlessly be able to stream notes and lists of notes (aka nodes with notes) between editor, treeview and stacklist.
            - Treeview is a sort of base structure here. And stack is a 'working memory' of sorts. Editor is then 'what is currently being processed'.
            - So this probably means that I want stack as an independent plugin so as to be replaceable/customizable
            - And that I want seamless streaming able to happen
        - Task List / Note List / Snippet Collection
          - Simpler than note organizer because it can all be done between stack and editor
          - Tasks written up and edited in editor
          - Tasks organized in stack, can be plucked out and set in according to mood and clicks after all
            - Need to use marker-divider in editor to separate tasks/notes from each other in this case.
              - Go with 'blank line' because it makes sense and is easily deleted
              - Default to pop last item in editor, but prioritize selected-area-of-text whenever it exists.
          - Stack should be able to sort and filter tasks based on hierarchical and freeform tags
          - Stack should be able to do both LIFO stack and FIFO queue
          - Extension options: get things from discord, marks for status (done/todo/in progress) and completion (progress/attempts/researched) in view
        - Architecture
          - Generalized streams can be done by each plugin listing themselves as sink handlers and source givers, plus only considering whichever plugin is activated at the moment
          - This would then mean each plugin generating an object with methods for calling up whichever functionality, being 'active' when shown and inactive when not shown
          - So plugin-find results in object (which has some state?) and calls a generator which yields the actual window
    - "So far so good" prototype
        - Interoperate by stack because that's what I did
        - Functionality
            - In/out for editor & treeview
            - fetch plugin for something
                - Discord or irc
                - Corona data
                - *SeeWhatIsEasy
        - This seems complex so I need to plan the fuck out of it

-----------------------------------------------------------------------------

Todo
-----

- Text editor should be able to pop and push data to/from main,
  and have copy-paste
- Same for Yaml editor, plus context menu
- Recognition of common data structures; messages, documents, weblinks,
  articles, tables, profiles etc
- View/Edit form of common data structures
- Communition module; to start with,
  'mail with token-code for filtering/recognition' for messaging
  and data exchange
- CRDT could be interesting and worthwhile
  State-based probably best since data, being plaintext and human-readable,
  is not intended to take up that much space.
  And more importantly, an asynchronous work flow does not guarantee text
  operations arriving in logical order.
  An asynchronous, collaborative editor that does not rely on websites..
  ..that could be nice
- How do I define a title in the plugin code and make sure that the main
  code display it nicely if it exists?
- Need: Delete option in stack component
- Need: Divider newline|line in editor for detecting/making lists; better editing
- Need: Ability to "Just save these notes here right now"
- Want: Persistent state UI/content

-----------------------------------------------------------------------------

(Re)Discoveries
----------------

Passed mutable objects can be changed no problem, and reassignment to new  
name passes the reference; only reassignment of new value overwrites name  
with new reference of the new object, destroying bond with outside  
object reference.  
Ergo, plugins can change a passed stack with no problem whatsoever, and then  
call a passed refresh function to the main application.  

Plugins work by placing them on a library path or in the same folder.  
pkgutil can automatically find them.  
Then you use importlib to load them.  

Functional Reactive Programming could be extremely applicable here.  
Discovered due to  
1: The stack-n-functions approach of the program  
2: My strong adversion to strongly coupling gui layer with other layers  
3: My strong adversion to let the gui framework dictate my coding style  
All in all, having components send and receive streams of data to each  
other and individually act on them is pretty much the premise of the program.  
Extending this to make it cleanly separate gui from core is only logical  
if it is possible.  
Functional Reactive Programming seems to hold this promise.  
The general principle is to have observers send and receive streams; having  
functions operate on these streams, perhaps as several functions composed  
together; and to have data and events generalized into standard streams so  
as for the various functions sort them out and handle all actions needed.  
Everything is functions and iterables(streams).  
State is handled with a separate, and probably global, state data structure.  
NB: Could also easily prove useful for later diversification of gui and os  
systems.  

Closures and closure issues.
So, closure is what happens to parameters to functions defined inline;
it's an issue in loops.
Closures made in a loop will - and according to me, for no discernible
reason - use the values for the last iteration of the loop for all
instances of functions/methods run.
As if it runs the loop and -then- repeats the loop for just the
function/methods. Odd.
Anyway, the way to deal with this is to use higher-order functions;
a function returning a function where the returned function uses parameters
from the scope of the creating function.
The closure then happens at the point of creation, and, importantly,
the returned function's side effects will happen as one would expect they
would, according to which step the loop is in.

Documentation.
The difficulty of learning something new is strongly affected by the
level of documentation available.
And the logicality of the structure of the given framework.
Current example being the difference between PyQt's.. or rather Qt's
data structures for data presentation and the documentation for it, as
compared to Python's documentation for the logging module.
I need the logging module to differentiate between themes now, because
of moving data between PyQt's widgets and my own data structures; it's
proving troublesome, and the logging I add to debug gets conflated with the
logging that is already in place for adding and removing items from the stack.
So I have both learning experiences in mind and have the opportunity to
compare.
Learning about the logging module is -much- simpler than Qt.
The logging module is logical. Its structure makes sense. Loggers, filters,
handlers, namespaces, logging levels. And they are arranged so that I don't
need to think about configuring any of those before I am at the point where
I actually need them.
Documentation-wise, the logging module has really good explanations,
introducing things a little at a time, mentioning more advanced subjects and
providing links for them if the interest is there, but keeping the beginning
to the introductory level, and then gradually expanding.
And there is a really nice diagram which explains the flow of logging events
through the system, making it all make sense.
And compared to this..
Qt's data model is somewhat convoluted to start with.
Example being, they have a Treeview widget. It is supposed to display
hierarchical data.
One would expect this data to be arranged in a typical hierarchical format,
but it isn't. Or rather, it is.. it is just really really oddly constructed.
For each node, its children are arranged in.. a spreadsheet format. Each node
has a table. And to simulate a tree structure, unless something really tricky
is going on, only the first column is used of that table.
The syntax reflects this. To work on the tree, one needs to consider rows and
columns.
Explanations for this are really hard to find. There is no comprehensive
introductory overview anywhere.
There is listing of classes and documentation of 30 functions for each of them,
and then there are tons of tutorials showing how to put a button in a window.
Trying to find out what is going on with this pretend-tree through dubious
examples writing tabular syntax without commenting on it (and that's for the
few examples that even goes into something this complex) is a struggle at
best.
Add to this that there's no overview of the Qt framework to find - the classes
are 'documented' in the sense that their functions are described, but there's
really not much in the way of explaining how they are supposed to be used.
To use a tree view, one has to declare a data model.
However, it is not explained that one then has to access data items through
the model, and not the view.
This is not really that surprising in retrospect, once confronted with the
knowledge, but while simply assuming that a graphical user interface presents
a view and that any data that needs to be contained is being so within the
widget itself.. it is quite confounding that there are no data access to be
found amongst it 30 functions.
And yet it is not entirely clear-cut.. since there are 'convenience functions'
and 'convenience classes' which bridges the separation between view and model
a bit because it is sometimes easier for developers to write things that way.
That also means, however.. that it is quite obscured what the concern of a
given class is supposed to be.
Overall.. figuring things out for Qt, slogging through web pages, perusing
class documentation, pondering obscure syntax and at last reaching out over irc
to make sense of it all.. I'd estimate that it is at -least- 10 times harder
than figuring out the logging module.
Which is, in comparison, really straightforward and only as complex as you need
it to be.

Python3's logging module
The logging module is lightweight, well structured, and flexible.
There -is- some sort of problem though.
Maybe related to this issue of logging settings either disabling all previous
loggers, or letting them be.
There seems to be some sort of all-or-nothing parameter being set somewhere.
Or maybe some naming scope issue.
Other than that, things are quite nice once you get into it.
Configurations can be done programmatically, by config file, or by dict - which
again can be instantiated via a config file created in yaml or json.
**Loggers** are what generates logging events. They have a log level setting
(debug, info, warning, error, critical) which is used to determine which level
of event they are willing to generate.
**Handlers** are what shows logging events, or writes them to files.
And handlers -also- have a log level setting - and it is this setting, not the
logger setting, which is used to limit output to console and/or files.
Aside from these, there are **Formatters** which styles the output;
and **Filters** which can be used to limit output on a more detailed level,
eg. per module or even function.

-----------------------------------------------------------------------------

Storytelling
-------------

So a thing called Obsidian is nearly out, and it is pretty close in concept,
except online-and-site based plus taking subscriptions.  
Their story is extremely short; and as usual for sites, very sparse on details.  

> **Take great notes**  
> **that outlive you.**  
> Finally, a safe vault for all your notes, in Markdown.  
> Start building your second brain today.  

> **Control your data**  
> Future-proof.  
> Obsidian works with plain text Markdown files that sit on your computer. No vendor lock-in, and zero risk of the service getting bought or shutting down.  
> Long-lasting.  
> If you're in it for the long haul, this is the single most important prerequisite for building your second brain.  

> **Connect the dots**  
> Together.  
> Link to any note with [[wiki style links]]. See how things are connected with backlinks and graph view.  
> Synergistic.  
> The network helps you discover previously unseen connections, and it offers branches for new knowledge to hang on to.  

> **It's all about you**  
> Personal.  
> Note-taking is an incredibly personal thing. There's truly no "one-size-fits-all" here, and we solve that by letting you tailor your own workflow.  
> Extensible.  
> We built Obsidian with extensibility in mind. Plugins are the building blocks — mix and match them to make Obsidian work for you.  

> **Ready to try Obsidian?**  

-----------------------------------------------------------------------------

Research
---------

- Scrivener
  - tool for writing books
  - similar-sounding functionality
    - "You can view your "chunks" as index cards or as text, whichever is more useful at the moment."
  - Paid-for, or maybe subscription-based
