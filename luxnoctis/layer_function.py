"""
Function Layer
Data filtering, searching, printing, processing, prettifying

The functions prettyform & shortform are used for presenting complex data
during debugging.

Import_plugins is used for locating and returning references to plugins,
for later loading.

The Observer class is used for exchanging messages between modules,
in particular between plugins and main application.
"""

import pprint
import pkgutil
import importlib
import logging


LOG = logging.getLogger(__name__)


def prettyform(obj):
    """
    Full print-out of supplied data structure.
    """
    return pprint.pformat(obj, indent=4)


def shortform(obj):
    """
    Abbreviated print-out of supplied data structure.
    """
    return pprint.pformat(obj, indent=4, depth=2)


def terseform(obj):
    """
    Returns only the first few lines of an object,
    just enough to recognize it.
    """
    data_str = pprint.pformat(obj, width=35, indent=1, depth=2)
    if len(data_str) > 100:
        data_str = data_str[:90] + "..."
    return data_str


def import_plugins():
    """
    Using standard libraries to locate related plugins through
    naming convention.
    """
    module = {}
    plugin_dir = 'plugins'
    plugin_pkg = importlib.import_module(plugin_dir)
    LOG.debug('Imported: %s', str(plugin_pkg))
    for finder, name, _ in pkgutil.iter_modules(plugin_pkg.__path__,
                                                plugin_pkg.__name__ + '.'):
        LOG.debug('Found: %s', name)
        if name.startswith('plugins.nox_'):
            mod = importlib.import_module(name, finder)
            module[name] = mod
            LOG.info("Plugin found: %s", name)
    return module


class Observer:
    """
    Listens to messages from other modules of the program and passes
    the notifications on, so that eg. the main application window can redraw
    the UI when new data has arrived.
    """
    def __init__(self):
        self.listeners = []

    def listen(self, callback):
        """Add caller to list of listeners."""
        self.listeners.append(callback)

    def leave(self, callback):
        """Remove caller from list of listeners."""
        if callback in self.listeners:
            self.listeners.remove(callback)

    def message(self, message_text):
        """Send message to all listeners."""
        LOG.debug(message_text)
        for msglistener in self.listeners:
            msglistener(message_text)


class PluginHandler:
    """
    Loads, unloads and keeps track of plugin components.
    Works by creating resource-light objects that can call the actual plugin
    components through .create functions.
    """
    def __init__(self, stack, observer):
        self.plugins_available = {}
        self.plugins_running = {}
        self.observer = observer
        self.stack = stack

    def load_plugins(self):
        """
        Load plugins found on system into each their own widget.
        """

        module = import_plugins()
        for name in module:
            try:
                plugin = module[name].Plugin()
            except AttributeError as attribute_error:
                plugin = None
                LOG.warning("Plugin load error : %s", name)
                if "Plugin" in str(attribute_error):
                    LOG.error("Missing required class Plugin")
            else:
                LOG.debug("Plugin load succes: %s", name)
            finally:
                if plugin:
                    self.plugins_available[name] = plugin

        LOG.info("Plugins loaded:")
        for plug in self.plugins_available:
            LOG.info(self.plugins_available[plug].name)
