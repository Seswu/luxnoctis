"""
Classes and functions pertaining to handling of data.

Stack class: Wraps a list in to coordinate between GUI, main and plugins.
"""
# pylint: disable=no-name-in-module
#         Pylint is unable to locate perfectly normal members of perfectly
#         normal modules. Explanations for this remain elusive.


# Standard modules
import logging

# Internal code
import luxnoctis.layer_function as layer_function


LOG = logging.getLogger(__name__)


class Stack:
    """
    The Stack class is quite simply a glorified list.
    It is intended to be used for passing data back and forth between the
    main module and the various plugins.
    It functions as a mediator. Some list functionality is deliberately
    withheld so as not to expose data to non-deliberate changes
    from the plugins.
    Other list functions are slightly changed so as to coordinate with the
    mediation function.

    As a mediator:
    Provide seamless access to two interdependent lists:
    One, the actual list,
    Two, the list of currently selected elements from that list.
    To do this, maintain an internal list of current selections,
    updated from the UI, and at the call of various list functions.
    Several list functions are changed, presenting the list of selections
    when one or more elements are selected, and defaults to giving the
    full list if no elements are selected.

    - if any items are in use, hinder access to them
    - if any items are selected, hinder access to unselected items
    - pass along selected items
    - pass along unselected items if no items are selected
    - fail reasonably if items are called for on an empty list
    - fail reasonably if specific items are not found
    """
    # pylint: disable=attribute-defined-outside-init
    # self.i is the iterator counter, it does not need definition in
    # __init__ since it is most sensibly defined in __iter__.

    def __init__(self, *args, observer=None):
        self.stacklist = list(args)
        self.selected = [False for element in range(len(self.stacklist))]
        self.observer = observer

    # Need some testing for multiple elements in one call; will they cause
    # the selected-list and the stacklist to go out of sync?
    def push(self, *args):
        """Add element to top of stack."""
        self.selected.append(False)
        self.stacklist.append(*args)
        self.observer.message("stack update")  # v-- todo: *args vs args?
        LOG.debug("Stack appended %s", layer_function.shortform(args))

#    def __contains__():
#    def count():
#    def index():
#    def extend():
#    def insert():
#    def remove():
#    def reverse():
#    def sort():

    def pop(self):
        """Remove and return element from top of stack."""
        try:
            self.selected.pop()
            value = self.stacklist.pop()
            self.observer.message("stack update")
            return value
        except IndexError as index_error:
            LOG.warning(
                "Operation requires more elements than data stack has; %s",
                str(index_error)
                )
            self.observer.message("status: Attempt to pop from empty stack")
            raise

    def get_selection_function(self, i):
        """
        Provide a function to set selection state for a given item.
        """
        def select(value):
            self.selected[i] = value
        return select

    def __iter__(self):
        """
        Initialize stack iterator, starting from top of stack.
        """
        length = len(self.stacklist)
        self.i = length - 1  # Start iterator at end of list/top of stack
        return self

    def __next__(self):
        """
        Get next item of stack; returns item and selection status, plus
        a function to set said selection status to a value of truth or false.
        """
        if self.i < 0:
            raise StopIteration
        item = self.stacklist[self.i]
        selected = self.selected[self.i]
        selection_function = self.get_selection_function(self.i)
        self.i -= 1
        return (item, selected, selection_function)
