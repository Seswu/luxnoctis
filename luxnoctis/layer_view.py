"""
View Layer
Defines graphic user interface

Mainwindow class runs main application, and loads all plugins discovered
on the system.
"""
# pylint: disable=no-name-in-module
#         Pylint is unable to locate perfectly normal members of perfectly
#         normal modules. Explanations for this remain elusive.


# Standard libraries
import logging

# PyQt5
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import (QMainWindow, QAction, QDockWidget, QVBoxLayout,
                             QPushButton, QWidget, QHBoxLayout, QLabel,
                             QToolButton, QGroupBox,
                             QSizePolicy, QScrollArea)

# Internal code
from luxnoctis.layer_function import Observer, terseform, PluginHandler
from luxnoctis.layer_data import Stack


LOG = logging.getLogger(__name__)


class StackLine(QGroupBox):
    """
    Class for displaying a single item of the stack.
    Aside from display, it also sets its selected/deselected status on the data
    layer by calling the supplied function at gui toggles of selection.
    """
    # pylint: disable=invalid-name, no-self-use
    # sizeHint is the name chosen by the PyQt module; it is just overloaded

    def __init__(self, item, selected, select):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setLayout(QHBoxLayout())
        self.layout().addWidget(QLabel(terseform(item)))
        self.button = QToolButton()
        self.button.setCheckable(True)
        self.button.setChecked(selected)

        def set_selection():
            select(self.button.isChecked())
        self.button.toggled.connect(set_selection)
        self.layout().addWidget(self.button)

    def sizeHint(self):
        """
        Recommended (fixed) size of this groupbox for gui.
        """
        return QSize(300, 50)


class StackPanel(QWidget):
    """
    Display of the current data stack.
    Updates itself and the observer object at changes to stack.
    """
    # pylint: disable=invalid-name, no-self-use
    # sizeHint is the name chosen by the PyQt module; it is just overloaded

    def __init__(self, stack, observer):
        super().__init__()
        self.observer = observer
        self.observer.listen(self.listen)
        self.stack = stack
        self.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
        check_button = QPushButton()
        check_button.setMaximumWidth(50)
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(check_button)
        scroll = QScrollArea(self)
        self.layout().addWidget(scroll)
        scroll.setWidgetResizable(True)
        scroll_content = QWidget(scroll)
        self.scroll_layout = QVBoxLayout(scroll_content)
        self.scroll_layout.addStretch(1)
        scroll_content.setLayout(self.scroll_layout)
        scroll.setWidget(scroll_content)
        self.add_stacklines()
        check_button.clicked.connect(self.print_stack)

    def delete_stacklines(self):
        """
        Empties layout of all stacklines
        """
        # There more logical way of going about this would be to call
        # a removeChildren method on the layout, or get a list of all its
        # children and then removing those one by one.
        # Unfortunately, such methods do not exist on layouts for PyQt,
        # and the ownership of children are ambiguous; layout().children()
        # returns an empty list, regardless of there being children added.
        # So, this method does it by index counting.
        layout = self.scroll_layout
        index = layout.count()
        while index > 1:  # item[0] is a spacer item, likely due to stretch()
            index -= 1
            LOG.debug("Item %i: %s", index, str(layout.itemAt(index)))
            widget = layout.itemAt(index).widget()
            widget.setParent(None)

    def add_stacklines(self):
        """
        Display all items currently in the stack, their selection status,
        and provide a binding function for updating that status from the gui.
        """
        for item, selected, select in self.stack:
            LOG.debug("Adding stackline; %s : %s : %s",
                      str(item), str(selected), str(select))
            linewidget = StackLine(item, selected, select)
            self.scroll_layout.addWidget(linewidget)

    def update(self):
        """
        Refresh display when stack is updated.
        """
        LOG.debug("Update stackpanel component")
        self.delete_stacklines()
        self.add_stacklines()
        super().update()
        self.show()

    def sizeHint(self):
        """
        Recommended size of this widget to gui.
        """
        # x=maximum, y=expanding
        return QSize(360, 500)

    def print_stack(self):
        """
        Debug function;
        Print out stack's internal list lengths for comparison; they should be
        equal.
        """

        LOG.debug(str([selected for _, selected, _ in self.stack]))
        LOG.debug("Stack: %i, Selected: %i",
                  len(self.stack.stacklist),
                  len(self.stack.selected))

    def listen(self, message_text):
        """
        Listen to messages from observer and take action if relevant.
        """
        if message_text == "stack update":
            self.update()


class MainWindow(QMainWindow):
    """
    Main window.

    Contains central widget to provide an overview and serve as data stack;
    Central widget and main menu provides actions for starting and stopping
    plugin modules, which in turn open new views and change data.
    Status bar in main window provides feedback.
    """

    def __init__(self):
        super().__init__()
        self.observer = Observer()
        self.stack = Stack(observer=self.observer)
        self.plugin_handler = PluginHandler(self.stack, self.observer)
        self.observer.listen(self.listen)
        self.plugin_handler.load_plugins()
        self.stackpanel = StackPanel(self.stack, self.observer)
        self.setCentralWidget(self.stackpanel)
        self.menu = {}
        self.init_data()
        self.init_menu()
        self.init_layout()
        self.init_ui()

    def __repr__(self):
        return "%s %s:%i" % (str(self.__class__),
                             self.appdata['title'],
                             len(self.stack.stacklist))

    def listen(self, message_text):
        """
        Listen to messages from observer and take action if relevant.
        """
        if message_text[:7] == "status:":
            self.statusBar().showMessage(message_text[8:])

    def init_data(self):
        """
        Define initial data.
        """
        self.appdata = \
            {
                'title': 'Lux Noctis',
                'left': 10,
                'top': 10,
                'width': 640,
                'height': 400
            }

    def init_layout(self):
        """
        Define layout of widgets in main window.
        """

        av_plugin = self.plugin_handler.plugins_available
        rn_plugin = self.plugin_handler.plugins_running

        # The menu slot functions that are called when the
        # user selects a given plugin needs to be defined
        # outside of the loop, because otherwise the python
        # compiler will (for some reason?) use the loop's last
        # calculated value for all instances of the function created.
        # Ie all menu items will call only the last plugin created.
        def create_calling_function(key):
            def call_plugin():
                rn_plugin[key] = \
                    av_plugin[key].create(self.stack, self.observer)
                if av_plugin[key].kind == "gui":
                    wrapper = QDockWidget(key, self)
                    self.addDockWidget(Qt.RightDockWidgetArea, wrapper)
                    wrapper.setWidget(rn_plugin[key])
            return call_plugin

        for pkey in av_plugin:
            plugin_button = QAction(av_plugin[pkey].action, self)
            if av_plugin[pkey].shortcut:
                plugin_button.setShortcut(av_plugin[pkey].shortcut)
            plugin_button.setStatusTip(av_plugin[pkey].tip)
            caller = create_calling_function(pkey)
            plugin_button.triggered.connect(caller)
            self.menu['plugin'].addAction(plugin_button)

    def init_menu(self):
        """
        Construct main menu.
        """
        # menu
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('&File')
        plugin_menu = main_menu.addMenu('&Plugins')
        self.menu = \
            {
                'main': main_menu,
                'file': file_menu,
                'plugin': plugin_menu,
            }

        # buttons
        exit_button = QAction('&Exit', self)
        exit_button.setShortcut('Ctrl+Q')
        exit_button.setStatusTip('Exit application')
        exit_button.triggered.connect(self.close)

        # file menu actions
        self.menu['file'].addAction(exit_button)

    def init_ui(self):
        """
        Initialize user interface.
        """
        self.setWindowTitle(self.appdata['title'])
        pos = (self.appdata['left'], self.appdata['top'])
        size = (self.appdata['width'], self.appdata['height'])
        self.setGeometry(*pos, *size)
        self.statusBar().showMessage('In progress')
