"""
File loading plugin
Loads the yaml file user asks for, and places its contents on the stack.
"""
# Standard libraries
import logging

# PyQt5
from PyQt5.QtWidgets import (QWidget, QFileDialog, QHBoxLayout,
                             QLabel, QMainWindow, QAction)
from PyQt5.QtGui import QIcon


class FileStream:
    """
    Handles file objects.
    Functions as source.
    Given a filename, loads corresponding data;
    (planned) Given data and filename, can store this as file.
    """

    def __init__(self, filename=None):
        self.data = None
        self.filename = filename

    def get_data(self):
        """
        Load data from file.
        """
        return_value = None
        if not self.filename:
            logging.error("No filename specified.")
        else:
            with open(self.filename, mode='r', encoding='utf-8') as stream:
                try:
                    return_value = stream.read()
                finally:
                    stream.close()
        return return_value

    def store_data(self, data):
        """
        Save changes to data.
        """
        if not self.filename:
            logging.error("No filename specified.")
        else:
            with open(self.filename, mode='w', encoding='utf-8') as stream:
                try:
                    stream.write(data)
                finally:
                    stream.close()


class StreamHandler(QMainWindow):
    """
    Stream handling component.
    """

    def __init__(self, stack, observer):
        super().__init__()
        self.title = "File Handler"
        self.stack = stack
        self.observer = observer
        self.capabilities = ['source']

        # QFileDialog returns list; first element is filename,
        # others are given parameters (in this case none aka defaults)
        filename = QFileDialog.getOpenFileName()[0]
        self.filestream = FileStream(filename)
        self.data = self.filestream.get_data()
        self.init_ui()
        self.create_toolbar()

    def init_ui(self):
        """
        Set up interface.
        """
        widget = QWidget()
        label = QLabel(widget)
        label.setText(self.filestream.filename)
        layout = QHBoxLayout()
        layout.addWidget(label)
        widget.setLayout(layout)
        self.setCentralWidget(widget)

    def create_toolbar(self):
        """
        Make an icon toolbar for use when eg pushing
        and popping data.
        """
        toolbar = self.addToolBar("Transfer")
        load_icon = QIcon("ui/bootstrap-icons/box-arrow-in-down.svg")
        store_icon = QIcon("ui/bootstrap-icons/box-arrow-up.svg")
        save_icon = QIcon("ui/bootstrap-icons/download.svg")
        load_action = QAction(load_icon, "load", self)
        store_action = QAction(store_icon, "store", self)
        save_action = QAction(save_icon, "save", self)
        toolbar.addAction(load_action)
        toolbar.addAction(store_action)
        toolbar.addAction(save_action)
        load_action.triggered.connect(self.push)
        store_action.triggered.connect(self.pop)
        save_action.triggered.connect(self.save)

    def push(self):
        """
        Append item from stack onto own data container.
        If there is already existing data.. replace it.
        """
        try:
            replacement = self.stack.pop()
        except IndexError:
            replacement = None
        if replacement:
            self.data = replacement

    def pop(self):
        """
        Put data from data contained on the stack.
        Actual 'pop' behaviour would also remove data from
        data container, but this makes little sense with files;
        user probably does -not- wish to delete data from their file.
        And if they do, pushing empty data onto the file instead
        would be a more obvious way to do so.
        So, streamhandler here functions like a never-ending source.
        """
        self.stack.push(self.data)

    def save(self):
        """
        Store internal data on disk.
        """
        self.filestream.store_data(self.data)


class Plugin:
    """
    Plugin factory for creating file handler components.
    """
    # pylint: disable=no-self-use, too-few-public-methods
    # Class is needed to find the plugin, .create is needed
    # on it to return the plugin component
    # (and leaving 'self' out as a parameter leads to other complaints)

    def __init__(self):
        self.name = "File Handler"
        self.action = "Open &file"
        self.shortcut = "Ctrl+O"
        self.tip = "Open a file and make its data available."
        self.kind = "gui"
        self.streamhandler = None

    def create(self, stack, observer):
        """
        Create and return file handler component.
        """
        return StreamHandler(stack, observer)
