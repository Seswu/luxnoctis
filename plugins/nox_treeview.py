"""
Treeview Plugin

Displays and makes editable hierarchical nodes of data.

Plugin class is the entry point used by main program;
TreeView class performs the heavy lifting aka data loading, conversion and,
in time, storing and editing.
debug function is a convenience function to avoid repetitive code;
used to debug data conversion to/from Qt's data format, which
can be hard to track.
"""

# Standard libraries
import collections
import logging
import pprint

# PyQt5
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import (QWidget, QTreeView, QVBoxLayout, QMenu, QTextEdit,
                             QMainWindow, QAction)
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon


LOG = logging.getLogger(__name__)


def debug(obj, decision=""):
    """
    Convenience function for debugging.

    Used for debugging data traversal when building up qt data model.
    """

    obj_shortform = pprint.pformat(obj, indent=4, depth=2)
    message = "\n{}\nType: {}".format(obj_shortform, type(obj))
    if decision:
        message += '\n' + decision
    LOG.debug(message)


class TreeView(QMainWindow):
    """
    Tree view display.

    Displays python data structures through Qt's treeview widget.
    """

    def __init__(self, stack, observer):
        super().__init__()

        self.widget = QWidget()
        self.setCentralWidget(self.widget)

        self.treeview = QTreeView()
        # self.treeview.setContextMenuPolicy(Qt.CustomContextMenu)
        # self.treeview.customContextMenuRequested.connect(self.open_menu)
        self.treeview.setContextMenuPolicy(Qt.ActionsContextMenu)

        self.model = QStandardItemModel()
        self.treeview.setModel(self.model)
        self.model.setHorizontalHeaderLabels([self.tr("Object")])

        layout = QVBoxLayout()
        layout.addWidget(self.treeview)
        self.widget.setLayout(layout)

        # Internal Coordination
        self.stack = stack
        self.observer = observer
        self.observer.listen(self.listen)

        # Toolbar
        toolbar = self.addToolBar("Stack")
        in_icon = QIcon("ui/bootstrap-icons/box-arrow-in-down.svg")
        out_icon = QIcon("ui/bootstrap-icons/box-arrow-up.svg")
        out_action = QAction(out_icon, "Out", self)
        in_action = QAction(in_icon, "In", self)
        toolbar.addAction(in_action)
        toolbar.addAction(out_action)
        in_action.triggered.connect(self.load)
        out_action.triggered.connect(self.pop)

        # Context Menu
        pop_action = QAction("Pop", self.treeview)
        pop_action.triggered.connect(self.pop)
        self.treeview.addAction(pop_action)
        # Getpos not active; not sure how to obtain positional data
        # getpos_action = QAction("Get Position", self.treeview)
        # getpos_action.triggered.connect(self.getpos)
        # self.treeview.addAction(getpos_action)

    def listen(self, message_text):
        """
        Listen to messages from observer and take action if relevant.
        """
        if message_text == 'yaml':
            self.load()

    def load(self):
        """
        Load data from stack into tree view.
        """
        try:
            self.add_items(self.model, self.stack.pop())
        except IndexError:  # Attempt to load from empty stack
            pass

    def get_current_item(self):
        """
        Find the current item in Qt's data model
        """
        # Aka find the current item in Qt's discombobulated
        # interface-focused mess of a data structure
        index = self.treeview.selectedIndexes()[0]
        current_item = index.model().itemFromIndex(index)
        return current_item

    def get_node(self, node):
        """
        Return given node and its subtree.
        """
        # Leaf
        if not node.hasChildren():
            leaf = node.text()
        if node.hasChildren():
            qtchildren = [node.child(row) for row in range(node.rowCount())]
            children = [self.get_node(qtchild) for qtchild in qtchildren]
            LOG.debug("Text of node: %s", node.text())
            if node.text() == '[]':
                # Key:value pair (todo - under [] seems faulty)
                if node.rowCount == 1:
                    key_value = children[0]
                # List
                list_of_nodes = children
            # Subtree
            subtree = {}
            subtree[node.text()] = children
        return leaf or key_value or list_of_nodes or subtree

    def pop(self):
        """
        Put the current item onto the stack, and remove it from the tree.
        """
        current_item = self.get_current_item()
        if current_item:
            LOG.debug("== Treeview item ==\n%s", str(current_item))
            self.stack.push(self.get_node(current_item))

    @pyqtSlot()
    def getpos(self, signal):
        """
        At right-click in data tree, display position of cursor in status line.
        """
        LOG.debug("Value %s", signal.data())
        LOG.debug("Row %s", signal.row())
        LOG.debug("Column %s", signal.column())
        first_line = str.split(str(signal.data()), '\n')[0]
        self.observer.message("status: %s (%s, %s)",
                              first_line, signal.row(),
                              signal.column())

    def add_items(self, parent, element):
        """
        Convert a python data structure into Qt's StandardItemModel format.
        """
        if isinstance(element, (float, int, str)):
            text = str(element)
            item = QStandardItem(text)
            parent.appendRow(item)
            debug(element, "== Add to treeview ==\n{}".format(text))
        elif isinstance(element, collections.Mapping):
            for key in element:
                try:
                    nodestr = str(key)
                    node = QStandardItem(nodestr)
                except Exception as exc:
                    msg = "Dictionary key incompatible with Qt data model.\n%s"
                    LOG.error(msg, exc)
                    raise ValueError("Object contains unexpected data.")
                parent.appendRow(node)
                LOG.debug("Node: %s", nodestr)
                self.add_items(node, element[key])
        elif isinstance(element, collections.Iterable):
            debug(element, "List")
            node = QStandardItem('[]')
            parent.appendRow(node)
            for ele in element:
                self.add_items(node, ele)
        else:
            LOG.debug("Element: %s\n",
                      pprint.pformat(element, indent=4))
            raise ValueError("Unrecognized data element; cannot convert")

    def open_menu(self, position):
        """
        Open right-click context menu.
        """
        menu = QMenu()
        edit_action = menu.addAction(self.tr("Edit"))
        action = menu.exec_(self.treeview.viewport().mapToGlobal(position))
        if action == edit_action:
            editor = QTextEdit()
            editor.setPlainText(
                "Once upon a time, a happy little lamb "
                "lived across the road.\n"
                "And this was why the chicken "
                "crossed the road from time to time."
            )
            editor.show()


class Plugin:
    """
    Plugin factory for creating treeview components.
    """
    # pylint: disable=no-self-use, too-few-public-methods
    # Class is needed to find the plugin, .create is needed
    # on it to return the plugin component
    # (and leaving 'self' out as a parameter leads to other complaints)
    def __init__(self):
        self.name = "Treeview"
        self.action = "&Treeview"
        self.shortcut = "Ctrl+T"
        self.tip = "View data in structured tree format"
        self.kind = "gui"

    def create(self, stack, observer):
        """
        Create and return editor component.
        """
        return TreeView(stack, observer)
