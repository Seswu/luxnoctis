"""
Yaml parsing plugin
Converts data between the yaml data format and python's internal data types.
"""
# Standard libraries
import logging
import yaml

# PyQt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton


LOG = logging.getLogger(__name__)


class YamlParser(QWidget):
    """
    Provides functions for converting data between yaml format and python
    data types.
    The yaml library does the actual heavy lifting; the raison d'etre for
    making this a plugin is compartmentalization; externalizing this
    functionality means that the user can convert data ad hoc, while other
    modules can focus on the data format that they expect.
    """

    def __init__(self, stack, observer):
        super().__init__()
        self.stack = stack
        self.observer = observer
        self.init_ui()

    def yaml_to_python(self):
        """
        Convert yaml text on stack to python data types.
        """
        try:
            data = yaml.safe_load(self.stack.pop())
            self.stack.push(data)
        except yaml.YAMLError as yaml_error:
            LOG.warning("Yaml data invalid: %s", yaml_error)
            self.observer.message("status: Yaml data invalid")
        except AttributeError as attribute_error:
            LOG.warning("Data is not readable: %s", attribute_error)
            self.observer.message("status: Data is not readable")
        except IndexError:
            pass  # Since stack was empty

    def python_to_yaml(self):
        """
        Convert python data on stack to yaml text.
        """
        try:
            data = self.stack.pop()
            result = yaml.dump(data, sort_keys=True)
            self.stack.push(result)
        except IndexError:
            pass  # Since stack was empty
        except yaml.YAMLError as exc:
            LOG.error("Error creating yaml data: %s", exc)

    def init_ui(self):
        """
        Create buttons in toolbar for access to conversion functions.
        """
        to_yaml = QPushButton("To Yaml")
        to_yaml.clicked.connect(self.python_to_yaml)
        from_yaml = QPushButton("From Yaml")
        from_yaml.clicked.connect(self.yaml_to_python)
        layout = QHBoxLayout(self)
        self.setLayout(layout)
        layout.addWidget(to_yaml)
        layout.addWidget(from_yaml)


class Plugin:
    """
    Plugin factory for creating file handler components.
    """
    # pylint: disable=no-self-use, too-few-public-methods
    # Class is needed to find the plugin, .create is needed
    # on it to return the plugin component
    # (and leaving 'self' out as a parameter leads to other complaints)

    def __init__(self):
        self.name = "Yaml Parser"
        self.action = "Y&aml parser"
        self.tip = "Create a parser to convert to/from the yaml text format."
        self.shortcut = None
        self.kind = "gui"

    def create(self, stack, observer):
        """
        Create and return file handler component.
        """
        return YamlParser(stack, observer)
