"""
Editor plugin
Shows a standard wysiwyg editor.
"""

# Standard libraries

# PyQt5
from PyQt5.QtWidgets import QTextEdit, QMainWindow, QAction
from PyQt5.QtGui import QIcon


class Editor(QMainWindow):
    """
    Run text editor.
    The editor is pretty run-of-the-mill wysiwyg but will be extended with
    some capabilities for pushing and popping data, and text inside it
    will be treated as a list of text snippets separated by markers.
    This marker will be a blank line as of the moment; perhaps later
    3-dashes-or-more, as in the markdown fashion.
    """
    def __init__(self, stack, observer):
        super().__init__()
        self.capabilities = ['editor']
        self.init_text = ""
        self.title = "Editor"
        self.stack = stack
        self.observer = observer
        self.editor = QTextEdit()
        self.editor.setPlainText(self.init_text)
        self.setCentralWidget(self.editor)
        self.create_toolbar()

    def create_toolbar(self):
        """
        Make an icon toolbar for use when eg pushing
        and popping data.
        """
        toolbar = self.addToolBar("Transfer")
        load_icon = QIcon("ui/fa-icons/sign-out-alt-solid.svg")
        store_icon = QIcon("ui/fa-icons/sign-in-alt-solid.svg")
        load_action = QAction(load_icon, "load", self)
        store_action = QAction(store_icon, "store", self)
        toolbar.addAction(load_action)
        toolbar.addAction(store_action)
        load_action.triggered.connect(self.push)
        store_action.triggered.connect(self.pop)

    def push(self):
        """
        Append item from stack onto the editor text,
        inserting divider as needed to maintain list
        structure.
        """
        text = self.editor.toPlainText()
        divider = self.create_divider()
        try:
            addition = self.stack.pop()
        except IndexError:
            addition = ""
            self.observer.message("status: Attempt to pop from empty stack")
        if not isinstance(addition, str):
            self.observer.message("status: Input for editor must be text")
            addition = ""
        if not addition:
            newtext = text
        else:
            if not text:
                newtext = addition
            else:
                newtext = text + divider + addition
        self.editor.setPlainText(newtext)

    def pop(self):
        """
        Remove last text item in the editor
        and put it on the stack instead.
        """
        text = self.editor.toPlainText()
        head, _, tail = text.rpartition('\n\n')
        head = head.strip()
        tail = tail.strip()
        if tail:
            self.stack.push(tail)
        self.editor.setPlainText(head)

    def create_divider(self):
        """
        String to use as divider to separate list items.
        """
        return "\n\n"


class Plugin:
    """
    Plugin factory for creating editor components.
    """
    # pylint: disable=no-self-use, too-few-public-methods
    # Class is needed to find the plugin, .create is needed
    # on it to return the plugin component
    # (and leaving 'self' out as a parameter leads to other complaints)
    def __init__(self):
        self.name = "Editor"
        self.action = "&Editor"
        self.shortcut = "Ctrl+E"
        self.tip = "Text file editor with stack capabilities"
        self.kind = "gui"

    def create(self, stack, observer):
        """
        Create and return editor component.
        """
        return Editor(stack, observer)
