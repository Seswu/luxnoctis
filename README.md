

Lux Noctis
===========

A collaboration tool in the making.  

There's grand intentions, of course, but for now it is merely a prototype
application that in the near future may be able to edit a few
markdown-formatted files in a roundabout manner.

Intentions: To become the swiss army chainsaw of online collaboration
----------------------------------------------------------------------

### The problem

Really, there's ample offers of things to make people work better together.  

But almost all of them require you to sign up somewhere and give strangers your
personal details.  
For the very few that doesn't, there is normally a time limit for the
persistence of your data, and in any case you are always depending on a
specific site, and a specific company, to continue providing you their
services.  

Of the paid options, they are often more complex than most small groups
actually need, while the free options are often too simple.  

For all of these, but especially for the chat networks, there is too much
bling, too much graphical layout taking up precious screen space, and not
enough focus on actually getting things done.

### The idea

The essence is simple:  
One central application acts as temporary data storage, plugins do the rest.  

Example 1:  
User A uses a text editor to make a markdown table to provide an overview
over who does what in their work group.  
Then they send that by a communication-plugin to said group.
User B uses same plugin to receive that data, but also uses a separate plugin
to display it as a kanban, because it's much easier to edit that way.  
Most of the rest of the group adopts the kanban-plugin, but for those that does
not, they can still see what's going on, either by text editor, in order to
edit as well (a simple plugin for this is already available) or through a
plugin to display markdown.  

Example 2:  
A plugin to handle discussions.  
Threaded overview of discussions are remarkably hard to find.  
(Yes, but we all know how mailing lists are not good a giving nice summaries)

- Decentralize
  - Don't rely on single sites
- Import and export of all data
  - Don't accept vendor lock-in
- Be simple
  - Don't get in the user's way
  - Don't assume the user's workflow
- Standard dataformats
  - It is more interoperable
  - It will allow for external tools to do the lifting
- Allow for radical extension
  - Be flexible
  - Users always want unexpected things
- Design for privacy
  - It's what the other solutions notably lack
  - Business decisions shouldn't be compromised
  - Minority groups should be able to avoid persecution
  - Journalists should be able to exchange information easily
  - Software security specialists collaborating on zero-day exploits should
    not have said exploits compromised before they have a fix ready and
    deployed
  - What my fictive cat does in its spare time isn't FB's business anyway

Installation / tryout
----------------------

Download or clone repository and run  
```
pip3 install -r requirements.lst
```
Then run luxnoctis.py  

Currently there is little functionality.  
Text editor loads a static text.  
One plugin can load a .yaml file which another plugin then displays in a
treeview.

Plans for the near future is to implement general data sharing between plugins
and visualization in the main program of the data stack, its contents and use
thereof.
